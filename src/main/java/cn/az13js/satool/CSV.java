package cn.az13js.satool;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSV {

    private String fileName = null;

    private int cacheSize = 40960;

    public CSV(String fileName) {
        this.fileName = fileName;
    }

    public double get(int line) throws FileNotFoundException, IOException {
        return get(line, 1);
    }

    public double get(int line, int col) throws FileNotFoundException, IOException {
        FileReader csv = new FileReader(fileName);
        // line >= 1 col >= 1
        int thisLine = 1;
        int thisCol = 1;
        char x = '\n';
        int readSize = 0;
        double result = 0.0;
        int lastPos = -1;
        int pos = 0;
        char[] cache = new char[cacheSize];
        StringBuilder fileContents = new StringBuilder(cacheSize);
        while ((readSize = csv.read(cache, 0, cacheSize)) > 0) {
            for (int i = 0; i < readSize; i++) {
                x = cache[i];
                switch (x) {
                    case ',':
                        if (thisLine == line && thisCol == col) {
                            result = Double.parseDouble(fileContents.toString());
                            csv.close();
                            return result;
                        }
                        fileContents.setLength(0);
                        thisCol++;
                        break;
                    case '\n':
                        if (lastPos + 1 != pos + i) {
                            if (thisLine == line && thisCol == col) {
                                result = Double.parseDouble(fileContents.toString());
                                csv.close();
                                return result;
                            }
                            thisLine++;
                            thisCol = 1;
                            lastPos = pos + i;
                        }
                        fileContents.setLength(0);
                        break;
                    default:
                        if (fileContents.length() >= cacheSize) {
                            fileContents.setLength(0);
                        }
                        fileContents.append(x);
                }
            }
            if (readSize < cacheSize) {
                break;
            }
            pos += readSize;
        }
        csv.close();
        return 0.0;
    }

    public int totalLine() throws FileNotFoundException, IOException {
        int total = 0;
        int readSize = 0;
        int pos = 0;
        int lastPos = -1;
        FileReader csv = new FileReader(fileName);
        char[] cache = new char[cacheSize];
        while ((readSize = csv.read(cache, 0, cacheSize)) > 0) {
            for (int i = 0; i < readSize; i++) {
                if ('\n' == cache[i]) {
                    if (lastPos + 1 != pos + i) {
                        total++;
                    }
                    lastPos = pos + i;
                }
            }
            if (readSize < cacheSize) {
                break;
            }
            pos += readSize;
        }
        csv.close();
        return total;
    }

}
